package si.ep.spletnatrgovina.interfaces;

public interface DialogFragmentDismissEvent {
    void dismissEvent();
}
