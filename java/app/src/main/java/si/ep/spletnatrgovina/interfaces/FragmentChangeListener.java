package si.ep.spletnatrgovina.interfaces;

import androidx.fragment.app.Fragment;

public interface FragmentChangeListener {
    void onFragmentChangeRequested(Fragment newFragment);
}
