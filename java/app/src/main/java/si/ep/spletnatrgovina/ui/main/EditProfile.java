package si.ep.spletnatrgovina.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import si.ep.spletnatrgovina.R;
import si.ep.spletnatrgovina.models.ProductResponse;
import si.ep.spletnatrgovina.models.User;
import si.ep.spletnatrgovina.retrofit.AuthAPI;
import si.ep.spletnatrgovina.retrofit.ProductsAPI;
import si.ep.spletnatrgovina.retrofit.ProfileAPI;
import si.ep.spletnatrgovina.retrofit.RetrofitFactory;

import static si.ep.spletnatrgovina.common.Common.BASE_URL;
import static si.ep.spletnatrgovina.common.Common.BEARER;

public class EditProfile extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private User user = null;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.field_name)
    EditText field_name;
    @BindView(R.id.field_mail)
    EditText field_mail;
    @BindView(R.id.field_gsm)
    EditText field_gsm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        setTitle(getString(R.string.edit_profile_title));

        setFields();
    }

    private void setFields() {
        String token = Objects.requireNonNull(
                PreferenceManager.getDefaultSharedPreferences(this)
                        .getString(BEARER, ""));

        Retrofit retrofit = RetrofitFactory.getInstance(BASE_URL);
        ProfileAPI profileAPI = retrofit.create(ProfileAPI.class);
        Call<User> call = profileAPI.getUserProfile(token);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {
                user = response.body();
                if (response.isSuccessful() && response.body() != null) {
                    field_name.setText(user.getName());
                    field_mail.setText(user.getEmail());
                    field_gsm.setText(user.getPhone());
                }
            }

            @Override
            public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {
                Log.d(TAG, "onFailure: " + t.getLocalizedMessage());
            }
        });
    }

    @OnClick(R.id.button)
    public void updateProfile() {
        String token = Objects.requireNonNull(
                PreferenceManager.getDefaultSharedPreferences(this)
                        .getString(BEARER, ""));

        Retrofit retrofit = RetrofitFactory.getInstance(BASE_URL);
        ProfileAPI profileAPI = retrofit.create(ProfileAPI.class);

        user.setName(field_name.getText().toString());
        user.setEmail(field_mail.getText().toString());
        user.setPhone(field_gsm.getText().toString());
        Call<String> call = profileAPI.updateUserProfile(user, token);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Toast.makeText(getApplicationContext(), "Spremembe shranjene.", Toast.LENGTH_LONG).show();
                    finish();
                }
            }

            @Override
            public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {
                Log.d(TAG, "onFailure: " + t.getLocalizedMessage());
            }
        });
    }
}
