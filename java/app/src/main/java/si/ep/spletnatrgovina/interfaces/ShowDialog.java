package si.ep.spletnatrgovina.interfaces;

import si.ep.spletnatrgovina.models.CartItem;

public interface ShowDialog {
    void showDialog(String itemName, CartItem item);
}
