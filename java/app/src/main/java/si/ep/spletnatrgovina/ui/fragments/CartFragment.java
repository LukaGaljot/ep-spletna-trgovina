package si.ep.spletnatrgovina.ui.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import si.ep.spletnatrgovina.BuildConfig;
import si.ep.spletnatrgovina.R;
import si.ep.spletnatrgovina.adapters.CartAdapter;
import si.ep.spletnatrgovina.interfaces.ChangeTitle;
import si.ep.spletnatrgovina.interfaces.DialogFragmentDismissEvent;
import si.ep.spletnatrgovina.interfaces.ShowDialog;
import si.ep.spletnatrgovina.models.Cart;
import si.ep.spletnatrgovina.models.CartItem;
import si.ep.spletnatrgovina.retrofit.CartAPI;
import si.ep.spletnatrgovina.retrofit.RetrofitFactory;
import si.ep.spletnatrgovina.ui.dialog.CartDialogFragment;
import si.ep.spletnatrgovina.viewmodels.CartViewModel;

import static si.ep.spletnatrgovina.common.Common.BASE_URL;
import static si.ep.spletnatrgovina.common.Common.BEARER;
import static si.ep.spletnatrgovina.common.Common.CART_ITEM_ID;
import static si.ep.spletnatrgovina.common.Common.QUANTITY;

public class CartFragment extends Fragment implements ShowDialog, DialogFragmentDismissEvent {
    private static final String TAG = CartFragment.class.getSimpleName();

    @BindView(R.id.cart_list_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;

    private CartViewModel cartViewModel;
    private CartAdapter cartAdapter;
    private Cart cart;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cart_fragment, container, false);
        ButterKnife.bind(this, view);

        if (getActivity() instanceof ChangeTitle) {
            ChangeTitle changeTitle = (ChangeTitle) getActivity();
            changeTitle.replaceTitle(getString(R.string.cart));
        } else {
            // Find your bugs early by making them clear when you can...
            if (BuildConfig.DEBUG) {
                throw new IllegalArgumentException("Fragment hosts must implement ChangeTitle");
            }
        }

        setHasOptionsMenu(true);

        cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);

        cart = new Cart();
        cart.setCart_items(new ArrayList<>());

        cartAdapter = new CartAdapter(cart, getContext(), this);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(getContext()), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(cartAdapter);

        fetchData();

        manageSwipeContainer();

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(@NotNull Menu menu) {
        menu.clear();
        menu.add(0, 123, Menu.NONE, getString(R.string.finish_order))
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    private void fetchData() {
        String token = Objects.requireNonNull(
                PreferenceManager.getDefaultSharedPreferences(getContext())
                        .getString(BEARER, ""));

        cartViewModel.getCart(token).observe(this, data -> {
            if (cart == null) return;
            cart = data;
            cartAdapter.setData(cart);
            cartAdapter.notifyDataSetChanged();

        });
    }

    private void getData() {
        String token = Objects.requireNonNull(
                PreferenceManager.getDefaultSharedPreferences(getContext())
                        .getString(BEARER, ""));

        Retrofit retrofit = RetrofitFactory.getInstance(BASE_URL);
        CartAPI cartAPI = retrofit.create(CartAPI.class);

        Call<Cart> call;
        call = cartAPI.getCart(token);
        call.enqueue(new Callback<Cart>() {
            @Override
            public void onResponse(@NotNull Call<Cart> call, @NotNull Response<Cart> response) {
                if (response.isSuccessful() && response.body() != null) {
                    cart = response.body();
                    cartAdapter.setData(cart);
                    cartAdapter.notifyDataSetChanged();
                } else {
                    Log.d(TAG, "Request failed\n" + response.errorBody());
                }
            }

            @Override
            public void onFailure(@NotNull Call<Cart> call, @NotNull Throwable t) {
                Log.e(TAG, t.getMessage(), t);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);

        fetchData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 123) {
            finishOrder();
        }

        return super.onOptionsItemSelected(item);
    }

    private void finishOrder() {
        String token = Objects.requireNonNull(
                PreferenceManager.getDefaultSharedPreferences(getContext())
                        .getString(BEARER, ""));

        Retrofit retrofit = RetrofitFactory.getInstance(BASE_URL);
        CartAPI cartAPI = retrofit.create(CartAPI.class);

        Call<String> call;
        call = cartAPI.completeOrder(token);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Toast.makeText(getContext(), "Naročilo uspešno zaključeno", Toast.LENGTH_LONG).show();
                    cart.setCart_items(new ArrayList<>());
                    cartAdapter.clearList();
                } else {
                    Log.d(TAG, "Request failed\n" + response.errorBody());
                }
            }

            @Override
            public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {
                Log.e(TAG, t.getMessage(), t);
            }
        });
    }

    @Override
    public void showDialog(String itemName, CartItem cartItem) {
        CartDialogFragment fragment = new CartDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(CART_ITEM_ID, cartItem.getId());
        bundle.putInt(QUANTITY, cartItem.getQuantity());
        fragment.setArguments(bundle);
        fragment.setListener(this);
        fragment.show(Objects.requireNonNull(getFragmentManager()), itemName);
    }

    @Override
    public void dismissEvent() {
        getData();
    }

    private void manageSwipeContainer() {
        swipeContainer.setOnRefreshListener(() -> {
            getData();
            swipeContainer.setRefreshing(false);
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }
}
