package si.ep.spletnatrgovina.interfaces;

public interface ChangeTitle {
    void replaceTitle(String title);
}
