package si.ep.spletnatrgovina.main;

import android.app.Application;

import com.facebook.stetho.Stetho;

public class App extends Application {
    private static final String TAG = App.class.getSimpleName();

    public App() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
