package si.ep.spletnatrgovina.viewmodels;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import si.ep.spletnatrgovina.models.ProductsResponse;
import si.ep.spletnatrgovina.retrofit.ProductsAPI;
import si.ep.spletnatrgovina.retrofit.RetrofitFactory;

import static si.ep.spletnatrgovina.common.Common.BASE_URL;

public class ProductsViewModel extends ViewModel implements Callback<ProductsResponse> {
    private static final String TAG = ProductsViewModel.class.getSimpleName();

    private MutableLiveData<ProductsResponse> products;

    private ProductsAPI productsAPI;
    private Retrofit retrofit;

    public LiveData<ProductsResponse> getProducts(String token) {
        if (retrofit == null) {
            retrofit = RetrofitFactory.getInstance(BASE_URL);
            productsAPI = retrofit.create(ProductsAPI.class);
        }

        if (products == null) {
            products = new MutableLiveData<>();
            load(token);
        }
        return products;
    }

    private void load(String token) {
        Call<ProductsResponse> call;
        call = productsAPI.getProducts(token);
        call.enqueue(this);
    }

    @Override
    public void onResponse(@NotNull Call<ProductsResponse> call, Response<ProductsResponse> response) {
        ProductsResponse productsResponse = new ProductsResponse();
        if (response.isSuccessful() && response.body() != null) {
            productsResponse.setData(response.body().getData());
        }
        products.setValue(productsResponse);
    }

    @Override
    public void onFailure(@NotNull Call<ProductsResponse> call, Throwable t) {
        Log.d(TAG, "onFailure: " + t.getLocalizedMessage());
    }
}
