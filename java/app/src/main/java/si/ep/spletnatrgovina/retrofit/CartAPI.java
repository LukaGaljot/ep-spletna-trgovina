package si.ep.spletnatrgovina.retrofit;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import si.ep.spletnatrgovina.models.Cart;

public interface CartAPI {
    @Headers("Accept: application/json")
    @GET("cart")
    Call<Cart> getCart(
            @Header("Authorization") String bearer
    );

    @Headers("Accept: application/json")
    @POST("cart")
    Call<String> addItemToCart(
            @Query("product_id") Integer product_id,
            @Header("Authorization") String bearer
    );

    @Headers("Accept: application/json")
    @DELETE("cart")
    Call<String> removeItemFromCart(
            @Query("cart_item_id") Integer cart_item_id,
            @Header("Authorization") String bearer
    );

    @Headers("Accept: application/json")
    @POST("cart/{cart_item_id}")
    Call<String> updateCartItem(
            @Path("cart_item_id") Integer cart_item_id,
            @Query("quantity") Integer quantity,
            @Header("Authorization") String bearer
    );

    @Headers("Accept: application/json")
    @POST("order")
    Call<String> completeOrder(
            @Header("Authorization") String bearer
    );
}
