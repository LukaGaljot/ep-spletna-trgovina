package si.ep.spletnatrgovina.models;

public class AuthResponse {
    private String bearer;

    public AuthResponse() {
    }

    public String getBearer() {
        return bearer;
    }

    public void setBearer(String bearer) {
        this.bearer = bearer;
    }
}
