package si.ep.spletnatrgovina.ui.fragments;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import si.ep.spletnatrgovina.R;
import si.ep.spletnatrgovina.models.ProductResponse;
import si.ep.spletnatrgovina.retrofit.CartAPI;
import si.ep.spletnatrgovina.retrofit.ProductsAPI;
import si.ep.spletnatrgovina.retrofit.RetrofitFactory;

import static si.ep.spletnatrgovina.common.Common.BASE_URL;
import static si.ep.spletnatrgovina.common.Common.BEARER;
import static si.ep.spletnatrgovina.common.Common.INTENT_PRODUCT_ID;

public class DetailFragment extends Fragment {
    private static final String TAG = DetailFragment.class.getSimpleName();

    private int product_id;

    @BindView(R.id.product_name)
    TextView product_name;

    @BindView(R.id.product_desc)
    TextView product_desc;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this, view);

        setHasOptionsMenu(true);

        addData();

        return view;
    }

    private void addData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            product_id = bundle.getInt(INTENT_PRODUCT_ID);
            Log.d(TAG, "Product id:" + product_id);

            String token = Objects.requireNonNull(
                    PreferenceManager.getDefaultSharedPreferences(getContext())
                            .getString(BEARER, ""));

            Retrofit retrofit = RetrofitFactory.getInstance(BASE_URL);
            ProductsAPI productsAPI = retrofit.create(ProductsAPI.class);
            Call<ProductResponse> call = productsAPI.getProduct(product_id, token);
            call.enqueue(new Callback<ProductResponse>() {
                @Override
                public void onResponse(@NotNull Call<ProductResponse> call, @NotNull Response<ProductResponse> response) {
                    ProductResponse productResponse = new ProductResponse();
                    if (response.isSuccessful() && response.body() != null) {
                        productResponse.setData(response.body().getData());
                        product_name.setText(productResponse.getData().getName());
                        product_desc.setText(productResponse.getData().getDescription());
                    }
                }

                @Override
                public void onFailure(@NotNull Call<ProductResponse> call, @NotNull Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getLocalizedMessage());
                }
            });
        }
    }

    @OnClick(R.id.add_to_cart)
    void addToCart() {
        String token = Objects.requireNonNull(
                PreferenceManager.getDefaultSharedPreferences(getContext())
                        .getString(BEARER, ""));

        Retrofit retrofit = RetrofitFactory.getInstance(BASE_URL);
        CartAPI cartAPI = retrofit.create(CartAPI.class);
        Call<String> call = cartAPI.addItemToCart(product_id, token);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), getString(R.string.added_to_cart), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getContext(), getString(R.string.error_adding_to_cart), Toast.LENGTH_LONG).show();
            }
        });
    }
}
