package si.ep.spletnatrgovina.models;

import java.util.List;

public class ProductsResponse {
    private List<Product> data;

    public ProductsResponse() {
    }

    public List<Product> getData() {
        return data;
    }

    public void setData(List<Product> data) {
        this.data = data;
    }
}
