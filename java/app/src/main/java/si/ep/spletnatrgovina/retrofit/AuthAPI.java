package si.ep.spletnatrgovina.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import si.ep.spletnatrgovina.models.AuthResponse;

public interface AuthAPI {
    @Headers("Accept: application/json")
    @GET("auth")
    Call<AuthResponse> getBearerToken(
            @Query("email") String email,
            @Query("password") String password
    );
}
