<?php

use App\ShoppingCart;
use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;

class AddUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up()
    {
        $names = ['Aljaž', 'Andraž', 'Anže', 'Belot', 'Benebod', 'Bogo', 'Bogomir', 'Bojan', 'Bor',
            'Boran', 'Borin', 'Borižit', 'Borut', 'Božjak', 'Branimir'];
        $lastnames = ['Novak', 'Horvat', 'Kovačič', 'Krajnc', 'Zupančič', 'Potočnik', 'Kovač', 'Mlakar',
            'Vidmar', 'Kos', 'Golob', 'Turk', 'Kralj', 'Božič', 'Korošec'];
        $area_code = ['031', '041', '051', '070', '069'];

        // Create customers
        for ($i = 0; $i < 30; $i++) {
            $name = $names[random_int(0, count($names) - 1)];
            $lastname = $lastnames[random_int(0, count($lastnames) - 1)];

            $phone = $area_code[random_int(0, 4)]
                . str_pad(random_int(0, 999), 3, '0', STR_PAD_LEFT)
                . str_pad(random_int(0, 999), 3, '0', STR_PAD_LEFT);
            $email = $this->createMail($name,$lastname);
            $this->createUser($name, $lastname, $phone, 1, '12345678',$email);
        }

        // Create customers
        for ($i = 0; $i < 12; $i++) {
            $name = $names[random_int(0, count($names) - 1)];
            $lastname = $lastnames[random_int(0, count($lastnames) - 1)];

            $phone = $area_code[random_int(0, 4)]
                . str_pad(random_int(0, 999), 3, '0', STR_PAD_LEFT)
                . str_pad(random_int(0, 999), 3, '0', STR_PAD_LEFT);
            $email = $this->createMail($name,$lastname);
            $this->createUser($name, $lastname, $phone, 2, '12345678',$email);
        }

        // Create admins
        $this->createUser("Luka","Galjot",051645750,3,'mocnogeslo',"luka@ep.si");
        $this->createUser("Jana","Štremfelj",051651124,2,'mocnonigeslo',"jana@ep.si");
        $this->createUser("Rok","Peterlin",051743234,2,'12345678',"rok@ep.si");
        $this->createUser("David","Jelenc",051734534,1,'kaksnogeslo',"david@ep.si");
    }

    private function createUser($name, $last, $phone, $role, $pass, $email) {
        try {
            $user = User::create([
                'name' => $name . " " . $last,
                'email' => $email,
                'phone' => $phone,
                'password' => Hash::make($pass),
                'role_id' => $role,
                'active' => true,
            ]);
            $cart = new ShoppingCart;
            $cart->user_id = $user->id;
            $cart->save();

            $user->cart_id = $cart->id;
            $user->save();
        } catch (Exception $e) {
            //
        }
    }

    private function createMail($name, $last) {
        $name = str_replace('č', 'c', $name);
        $last = str_replace('č', 'c', $last);
        $name = str_replace('š', 's', $name);
        $last = str_replace('š', 's', $last);
        $name = str_replace('ć', 'c', $name);
        $last = str_replace('ć', 'c', $last);
        $name = str_replace('ž', 'z', $name);
        $last = str_replace('ž', 'z', $last);

        return strtolower($name) . "." . strtolower($last) . "@ep.si";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
