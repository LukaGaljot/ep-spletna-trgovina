<?php

use App\Product;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    public function run()
    {
        Product::query()->truncate();
        $fake = Factory::create();

        for($x=0; $x < 100; $x++){
            Product::query()->create([
                "name" => $fake->words(2, true),
                "price" => $fake->randomFloat(2,0.1, 50),
                "description" => $fake->paragraph
            ]);
        }
    }
}
