<?php

use App\Http\Resources\ProductResource;
use App\Product;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('auth', 'Api\ApiAuthController@authenticate');

Route::get('products', function () {
    return new ProductResource(Product::where('active', true)->get());
});

Route::get('products/{product_id}', function ($product_id) {
    return new ProductResource(Product::find($product_id));
});

Route::fallback(function () {
    return response('Not Found', 404);
});

Route::get('cart', 'Api\ApiCartController@getCart');
Route::post('cart', 'Api\ApiCartController@addItemToCart');
Route::post('cart/{cart_item_id}', 'Api\ApiCartController@updateCartItem');
Route::delete('cart', 'Api\ApiCartController@removeItemFromCart');

Route::post('order', 'Api\ApiOrderController@finishOrder');

Route::get('user', 'Api\ApiUserController@getUser');
Route::post('user', 'Api\ApiUserController@updateUser');

