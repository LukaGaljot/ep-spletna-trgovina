<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/cert_login', 'CertLoginController@index');

Route::get('/dashboard', 'DashboardController@index');

Route::get('/dashboard/customers', 'DashboardController@getCustomers');

Route::get('/dashboard/sellers', 'DashboardController@getSellers');

Route::get('/dashboard/customers', 'DashboardController@getCustomers');

Route::get('/dashboard/users', 'DashboardController@getAll');

Route::post('/dashboard/updateUser/{id}', 'ProfileController@updateUser');

Route::get('/dashboard/logs','DashboardController@getAllLogs');

Route::get('/dashboard/logs/{name}','DashboardController@readLog');

Route::get('/dashboard/orders','OrderController@getAllOrders');

Route::get('/dashboard/{id}','DashboardController@getUser');

Route::get('/profile', 'ProfileController@profile');

Route::get('/profile/edit', 'ProfileController@edit_info');

Route::post('/profile/update', 'ProfileController@update');

Route::post('/profile/active', 'ProfileController@toggleActivity')->middleware('auth.x509.roles');

Route::get('/', 'ProductsController@index');

Route::post('products', 'ProductsController@store')->middleware('auth.x509.roles');
Route::get('products', 'ProductsController@index');
Route::get('products/create', 'ProductsController@create')->middleware('auth.x509.roles');
Route::delete('products/{product}', 'ProductsController@destroy')->middleware('auth.x509.roles');
Route::put('products/{product}', 'ProductsController@update')->middleware('auth.x509.roles');
Route::get('products/{product}', 'ProductsController@show');
Route::get('products/{product}/edit', 'ProductsController@edit')->middleware('auth.x509.roles');

Route::get("product/rate/{id}", 'ProductsController@getRating');

Route::post("product/rate/{id}", 'ProductsController@rate')->middleware('auth');

Route::get("product/{id}/pictures", 'ProductsController@pictures')->middleware('auth.x509.roles');

Route::post("product/{id}/pictures", 'ImageController@create')->middleware('auth.x509.roles');

Route::delete("product/{id}/pictures", 'ImageController@delete')->middleware('auth.x509.roles');

Route::get('/search', 'ProductsController@search');

Route::post('/cart/add', 'CartController@addToCart');

Route::resource('/cart', 'CartController');

Route::get('orders','OrderController@getAllOrdersByUser')->middleware('auth');

Route::post('order','OrderController@finishOrder')->middleware('auth');

Route::get('order/{id}','OrderController@getOrder')->middleware('auth');

Route::post('order/{id}','OrderController@changeStatus')->middleware('auth.x509.roles');

Route::get('create', 'Auth\RegisterController@create');

Route::post('captcha', 'Auth\RegisterController@validator');

Route::get('refreshCaptcha','Auth\RegisterController@refreshCaptcha');
