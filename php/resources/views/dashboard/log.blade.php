@extends('layouts.app')

@section('content')
      <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-lg-9 col-sm-12">
                    <h1 class="">{{$title}}</h1>
                    <div class="card">
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            @if($contents != null && count($contents) > 0)
                                <ul class="list-group">
                                    @foreach($contents as $line)
                                        <li class="list-group-item d-flex justify-content-left align-items-center">
                                            <p class="text-info">{{$line}}</p>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
      </div>
@endsection
