@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-9 col-sm-11">
                <h1 class="">{{$title ?? ''}} {{$user->name}}</h1>
                <div class="card">
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if($user != null)
                            <form method="POST" action="/dashboard/updateUser/{{$user->id}}">
                                @csrf
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Ime in priimek') }}</label>
                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$user->name}}" required autocomplete="name" autofocus>
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail naslov') }}</label>
                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$user->email}}" required autocomplete="email">
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Telefonska številka') }}</label>
                                    <div class="col-md-6">
                                        <input id="phone" type="tel" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{$user->phone}}" required autocomplete="phone">

                                        @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                @if($role==3)
                                    <div class="form-group row">
                                        <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('Vloga') }}</label>
                                        <div class="col-md-6">
                                            <select id="role" class="form-control" name="role"required>
                                                @if($user->role_id==1)
                                                    <option value="{{$user->role_id}}">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                @elseif($user->role_id==2)
                                                    <option value="{{$user->role_id}}">2</option>
                                                    <option value="1">1</option>
                                                    <option value="3">3</option>
                                                @else
                                                    <option value="{{$user->role_id}}">3</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group row mb-0">
                                   <div class="col-md-6 offset-md-4">
                                       <button type="submit" class="btn btn-primary" style="margin-bottom:5px;">
                                           {{ __('Posodobi podatke') }}
                                       </button>
                                    </div>
                                </div>
                        @else
                            Uporabnik ne obstaja.
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
