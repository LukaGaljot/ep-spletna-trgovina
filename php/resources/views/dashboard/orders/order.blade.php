@extends('layouts.app')

@section('content')
      <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-lg-9 col-sm-12">
                    <h1 class="">{{$title}}</h1>
                    <h6>Datum: {{$order->created_at}}</h6>

                    <div class="card">
                        <div class="card-body">

                            <ul class="list-group">
                                 @foreach ($order->orderItems as $item)
                                    <h5 class="">Produkt: <a href="/products/{{$item->product->id}}">{{$item->name}}</a></h5>
                                    <h6 class="">Cena produkta: {{$item->price}} €</h6>
                                    <h6 class="">Količina: {{$item->quantity}} x</h6>
                                    <h6 class="">Skupna cena: {{$item->getPriceSum()}} €</h6>
                                    <hr>
                                 @endforeach
                                 <h4  class="text-right">Cena računa: {{$order->sumItemsPrices()}} €</h4>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
      </div>
@endsection
