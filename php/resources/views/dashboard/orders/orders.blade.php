@extends('layouts.app')

@section('content')
      <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-lg-10 col-sm-12">
                    <h1 class="">{{$title}}</h1>

                    <div class="card">
                        <div class="card-body">

                            @if($orders != null && count($orders) > 0)
                                <ul class="list-group">
                                    @foreach($orders as $o)
                                        <li class="list-group-item justify-content-between align-items-center">
                                            <h5>Naročilo {{$o->id}}</h5>
                                            <h6>Datum: {{$o->created_at}}</h6>
                                            <h6>Stanje: {{$o->status->name}}</h6>

                                            @if($o->status->id == 1)
                                                <div class="form-inline">
                                                    {!! Form::open(['action' => ['OrderController@changeStatus', $o->id], 'method' => 'POST', 'class' => 'mb-1']) !!}
                                                    {{ Form::hidden('action', 'confirm')}}
                                                    {{ Form::submit('Potrdi', ['class' => 'btn btn-sm btn-success'])}}
                                                    {!! Form::close() !!}

                                                    {!! Form::open(['action' => ['OrderController@changeStatus', $o->id], 'method' => 'POST', 'class' => 'mb-1 mr-1 ml-1']) !!}
                                                    {{ Form::hidden('action', 'remove')}}
                                                    {{ Form::submit('Zavrni', ['class' => 'btn btn-sm btn-danger'])}}
                                                    {!! Form::close() !!}
                                                </div>
                                            @endif

                                            @if($o->status->id == 2)
                                                {!! Form::open(['action' => ['OrderController@changeStatus', $o->id], 'method' => 'POST', 'class' => 'mb-1']) !!}
                                                {{ Form::hidden('action', 'cancel')}}
                                                {{ Form::submit('Storniraj', ['class' => 'btn btn-sm btn-danger', "style" => "width:109px"])}}
                                                {!! Form::close() !!}
                                            @endif

                                            <a role="button" class="btn btn-sm btn-info" href="/order/{{$o->id}}"
                                                style="width:109px">Preglej naročilo</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
      </div>
@endsection
