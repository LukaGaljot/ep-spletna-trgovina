@extends('layouts.app')

@section('content')
      <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <h1 class="">{{$title}}</h1>

                    <div class="card">
                        <div class="card-body">

                            @if(count(Auth::user()->orders) > 0)
                                <ul class="list-group">
                                    @foreach(Auth::user()->orders as $o)
                                        <li class="list-group-item justify-content-between align-items-center">
                                            <h4>Naročilo {{$o->id}}</h4>
                                            <small>Datum: {{$o->created_at}}</small><br>
                                            <h7>Stanje: {{$o->status->name}}</h7><br>
                                            <a role="button" class="btn btn-sm btn-info mt-2" href="/order/{{$o->id}}">Preglej naročilo</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <h5>Ni preteklih naročil</h5>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
      </div>
@endsection
