@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="">{{$title}}</h1>
                <h6>Vrsta profila: <b>{{Auth::user()->role->name}}</b></h6>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-sm-12 col-md-6 col-lg-4 mb-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Urejanje naročil</h5>
                        <a href="/dashboard/orders" role="button" class="btn btn-primary">Naročila</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-4 mb-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Urejanje uporabnikov</h5>
                        @if (Auth::user()->isAdmin())
                            <a href="/dashboard/users" role="button" class="btn btn-primary">Uporabniki</a>
                            <a href="/dashboard/sellers" role="button" class="btn btn-primary">Prodajalci</a>
                        @endif
                        @if (Auth::user()->isSeller() || Auth::user()->isAdmin())
                            <a href="/dashboard/customers" role="button" class="btn btn-primary">Stranke</a>
                        @endif
                    </div>
                </div>
            </div>

            @if(Auth::user()->isAdmin())
                <div class="col-sm-12 col-md-6 col-lg-4 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Preglej dnevnik</h5>
                            <a href="/dashboard/logs" role="button" class="btn btn-primary">Dnevniki</a>
                        </div>
                    </div>
                </div>
            @endif
        </div>

    </div>
@endsection
