@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-9 col-sm-11">
                <h1 class="">{{$title}}</h1>

                <div class="card">
                    <div class="card-body">
                        @if($customers != null && count($customers) > 0)
                            <ul class="list-group">
                                @foreach($customers as $customer)
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <p class="p-0 m-0 flex-grow-1">{{$customer->name}}</p>
                                        <div>
                                            <span class="badge badge-primary badge-pill mr-4">Stranka</span>
                                            <a role="button" class="btn btn-sm btn-success pull-right" href="/dashboard/{{$customer->id}}">Uredi
                                                podatke</a>
                                            @if($customer->active == true)
                                                {!! Form::open(['action' => ['ProfileController@toggleActivity'], 'method' => 'POST', 'class' => 'float-right pl-1']) !!}
                                                {{ Form::hidden('action', 'deactivate')}}
                                                {{ Form::hidden('user_id', $customer->id)}}
                                                {{ Form::submit('Deaktiviraj', ['class' => 'btn btn-sm btn-danger'])}}
                                                {!! Form::close() !!}
                                            @else
                                                {!! Form::open(['action' => ['ProfileController@toggleActivity'], 'method' => 'POST', 'class' => 'float-right pl-1']) !!}
                                                {{ Form::hidden('action', 'activate')}}
                                                {{ Form::hidden('user_id', $customer->id)}}
                                                {{ Form::submit('  Aktiviraj  ', ['class' => 'btn btn-sm btn-info'])}}
                                                {!! Form::close() !!}
                                            @endif
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            {{$customers->links()}}
                        @else
                            Ni registriranih strank.
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
