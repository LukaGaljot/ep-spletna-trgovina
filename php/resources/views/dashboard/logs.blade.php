@extends('layouts.app')

@section('content')
      <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-lg-9 col-sm-12">
                    <h1 class="">{{$title}}</h1>
                    <div class="card">
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            @if($logs != null && count($logs) > 0)
                                <ul class="list-group">
                                    @foreach($logs as $logIndex => $logName)
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            <div>
                                                <span class="badge badge-info badge-pill mr-4">Log name </span>
                                                <a role="button" class="btn btn-sm btn-success pull-right" href="/dashboard/logs/{{$logName}}">{{$logName}}</a>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
      </div>
@endsection
