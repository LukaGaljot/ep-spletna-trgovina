@extends('layouts/app')
@section('content')
    <h1 class="mt-5">Uredi produkt</h1>
    <!-- https://laravelcollective.com/docs/6.0/html -->
    {!! Form::open(['action' => ['ProductsController@update', $product->id], 'method' => 'post']) !!}
    <div class="form-group">
        {{Form::label('name', 'Name')}}
        {{Form::text('name', $product->name, ['class' => 'form-control', 'placeholder' => 'Name'])}}
    </div>
    <div class="form-group">
        {{Form::label('price', 'Price')}}
        {{Form::text('price', $product->price, ['class' => 'form-control', 'placeholder' => 'Product price'])}}
    </div>
    <div class="form-group">
        {{Form::label('description', 'Description')}}
        {{Form::textarea('description', $product->description, ['class' => 'form-control', 'placeholder' => 'Product description'])}}
    </div>
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
