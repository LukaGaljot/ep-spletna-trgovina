@extends('layouts.app')
@section('headers')
    <link rel="stylesheet" href="{{asset('css/uploadButton.css')}}"
@section('content')
      <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-lg-9 col-sm-12">
                    <h1 class="">{{$title}}</h1>
                    <div class="card">
                        <div class="card-body">
                            <ul class="list-group">
                                @foreach($product->images as $img)
                                    <li class="list-group-item justify-content-between align-items-center">
                                        <img src="/storage/images/{{$img->image}}"/>
                                         {!! Form::open(['action' => ['ImageController@delete',$img->id], 'method' => 'POST', 'class' => 'form-inline']) !!}
                                            {{Form::submit('Delete picture',['class' => 'btn btn-danger']) }}
                                            {{Form::hidden('_method',"DELETE")}}
                                         {!! Form::close() !!}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-9 col-sm-12">
                    <h1>Dodajanje nove slike</h1>
                        {!! Form::open(['action' => ['ImageController@create',$product->id], 'method' => 'POST', 'enctype' =>'multipart/form-data', 'class' => 'form-inline']) !!}
                            {{Form::file('image',['id'=>"file"])}}
                            {{Form::label('file','File',['class' => 'btn-2'] )}}
                            {{Form::hidden('product_id',$product->id)}}
                            {{Form::submit('Publish picture',['class' => 'btn btn-success btn-2',"style"=>"height: 100%;"]) }}
                        {!! Form::close() !!}
                </div>
            </div>
      </div>
@endsection
