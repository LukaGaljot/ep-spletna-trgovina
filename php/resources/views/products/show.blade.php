@extends('layouts/app')
@section('headers')
    <script src="{{ asset('js/stars.js') }}" defer></script>
    <link rel="stylesheet" href="{{asset('css/stars.css')}}"/>
@endsection

@section('content')
    @if ($product != null)
        <div class="row">
            <div class="col-auto mr-auto rating">
                <h1 class="mt-5">{{$product->name}}</h1>
                <div id="stars"></div>
            </div>

            <div class="col-auto">
                @if (!Auth::guest() && Auth::user()->role_id > 1)
                    <a href="/products/{{$product->id}}/edit" class="mt-5 mr-1 btn btn-dark">Uredi</a>
                    <a href="/product/{{$product->id}}/pictures" class="mt-5 mr-1 btn btn-dark">Uredi slike</a>
                    {!! Form::open(['action' => ['ProductsController@destroy', $product->id], 'method' => 'POST', 'class' => 'float-right']) !!}
                    {{ Form::hidden('_method', 'DELETE')}}
                    {{ Form::submit('Izbriši', ['class' => 'mt-5 btn btn-danger'])}}
                    {!! Form::close() !!}
                @endif
                @if(!Auth::guest() && Auth::user()->role_id > 1)
                        @if($product->active)
                            {!! Form::open(['action' => ['ProductsController@update', $product->id], 'method' => 'POST', 'class' => 'float-right']) !!}
                            {{ Form::hidden('active', 0) }}
                            {{ Form::hidden('action', 'deactivated') }}
                            {{ Form::hidden('_method', 'PUT')}}
                            {{ Form::submit('Deaktiviraj', ['class' => 'mt-5 mr-1 btn btn-secondary'])}}
                            {!! Form::close() !!}
                        @else
                            {!! Form::open(['action' => ['ProductsController@update', $product->id], 'method' => 'POST', 'class' => 'float-right']) !!}
                            {{ Form::hidden('active', 1) }}
                            {{ Form::hidden('action', 'activated') }}
                            {{ Form::hidden('_method', 'PUT')}}
                            {{ Form::submit('Aktiviraj', ['class' => 'mt-5 mr-1 btn btn-success'])}}
                            {!! Form::close() !!}
                        @endif
                @endif
            </div>
        </div>
        <br>
        <small>{{$product->description}}</small>
        <br>
        <div>
            <h3 class="text-right">{{$product->price}} €</h3>
        </div>

{{--        <a href="/cart/{{$product->product_id}}" class="mt-5 mr-1 btn btn-outline-info">Dodaj v košarico</a>--}}
        <div>
            @if(!Auth::guest())
                {!! Form::open(['action' => ['CartController@addToCart'], 'method' => 'POST', 'class' => 'form-inline']) !!}
                {{ Form::number('quantity', 1, ['class' => 'form-control mr-2 col-3']) }}
                {{ Form::hidden('product_id', $product->id) }}
                {{ Form::submit('V košarico', ['class' => 'btn btn-primary col-3']) }}
                {!! Form::close() !!}
            @endif
        </div>
         <div>
            @foreach($product->images as $img)
                 <div class="row m-3"><img src="/storage/images/{{$img->image}}"style="max-width:500px;height: auto;" /></div>
             @endforeach
         </div>
    @else
        <p>Post not found</p>
    @endif
@endsection
