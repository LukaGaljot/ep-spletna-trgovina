@extends('layouts/app')

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="">{{$title}}</h1>

                <div class="card">
                    <div class="card-body">
                        <small class="text-muted">Vloga</small>
                        <p>{{ Auth::user()->role->name }}</p>

                        <small class="text-muted">Ime in priimek</small>
                        <p>{{ Auth::user()->name }}</p>

                        <small class="text-muted">E-Mail</small>
                        <p>{{ Auth::user()->email }}</p>

                        <small class="text-muted">Telefonska številka</small>
                        <p>{{ Auth::user()->phone }}</p>

                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8 mt-2">
                <a href="/password/reset" class="btn btn-outline-danger float-right">Ponastavi geslo</a>
                <a href="/profile/edit/" class="btn btn-outline-secondary float-left">Uredi podatke</a>
            </div>
        </div>
    </div>

@endsection
