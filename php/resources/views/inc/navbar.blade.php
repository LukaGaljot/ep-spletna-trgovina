<nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
    <a class="navbar-brand" href="{{ url('/') }}">
        {{ config('app.name', 'Laravel') }}
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Left Side Of Navbar -->
        @if(!Auth::guest())
            <ul class="navbar-nav mr-0">
                <li class="nav-item">
                    <a class="nav-link" href="/cart">Košarica 🛒</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-0 mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/orders">Pretekli nakupi</a>
                </li>
            </ul>
        @endif

        @if(!request()->is('*login') && !request()->is('*register'))
            <ul class="navbar-nav ml-auto mr-4">
                <li class="nav-item">
                    {!! Form::open(['action' => ['ProductsController@search'], 'method' => 'GET', 'class' => 'form-inline my-2 my-lg-0 ml-auto']) !!}
                    {{ Form::text('term', $query ?? '', ['class' => 'form-control mr-sm-2']) }}
                    {{ Form::submit('Išči', ['class' => 'btn btn-outline-success my-2 my-sm-0']) }}
                    {!! Form::close() !!}
                </li>
            </ul>
        @endif

        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-0 mr-2">
            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        @if(!Auth::user()->isCustomer())
                            <a class="dropdown-item" href="/dashboard">
                                Nadzorna plošča
                            </a>
                        @endif

                        <a class="dropdown-item" href="/profile">
                            Profil
                        </a>

                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {{ __('Odjava') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
    </div>
</nav>
