@if(count($errors) > 0)
@foreach ($errors->all() as $error)
    <div class="alert alert-danger mt-2">
        {{$error}}
    </div>
@endforeach
@endif

@if(session('success'))
    <div class="alert alert-success mt-2 alert-dismissible fade show">
        {{session('success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger mt-2 alert-dismissible fade show">
        {{session('error')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
