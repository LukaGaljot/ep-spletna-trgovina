@extends('layouts/app')
@section('content')
    <div class="row">
        <div class="col-auto mr-auto">
            <h1 class="mt-5">Košarica</h1>
        </div>

        @if(!Auth::guest() && count($items) > 0)
            <div class="col-auto">
                {!! Form::open(['action' => ['OrderController@finishOrder'], 'method' => 'POST', 'class' => '']) !!}
                {{ Form::submit('Zaključi nakup', ['class' => 'mt-5 btn btn-dark'])}}
                {!! Form::close() !!}
            </div>
        @endif
    </div>

    <div class="row">
        @if (count($items) >= 1)
            @foreach ($items as $item)
                <div class="col-md-4 pl-0">
                    <div class="card card-body bg-light mb-2 mr-2">

                        <div class="row">
                            <div class="col-auto mr-auto">
                                <h3><a href="/products/{{$item->product->id}}">{{$item->product->name}}</a></h3>
                            </div>

                            {!! Form::open(['action' => ['CartController@destroy', $item->id], 'method' => 'POST', 'class' => 'float-right']) !!}
                            {{ Form::hidden('_method', 'DELETE')}}
                            {{ Form::submit('&times;', ['class' => 'btn close'])}}
                            {!! Form::close() !!}
                        </div>

                        <div class="row mt-4">
                            <div class="col-12">
                                <h3>{{$item->product->price}} €</h3>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 form-inline">
                                {!! Form::open(['action' => ['CartController@update', $item->id], 'method' => 'POST', 'class' => 'float-right']) !!}
                                {{ Form::hidden('_method', 'PUT')}}
                                {{ Form::hidden('action', 'remove') }}
                                {{ Form::submit('-', ['class' => 'btn btn-outline-secondary'])}}
                                {!! Form::close() !!}
                                <label class="col-form-label col-form-label-lg ml-3 mr-3">{{$item->quantity}}</label>
                                {!! Form::open(['action' => ['CartController@update', $item->id], 'method' => 'POST', 'class' => 'float-right']) !!}
                                {{ Form::hidden('_method', 'PUT')}}
                                {{ Form::hidden('action', 'add') }}
                                {{ Form::submit('+', ['class' => 'btn btn-outline-secondary'])}}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        @else
            <div class="col-12 text-center">
                <h5 class="text-center">V košarici ni izdelkov.</h5>
            </div>
        @endif
    </div>

@endsection
