<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingCartItem extends Model
{
    // table name
    protected $table = 'shopping_cart_items';

    protected $fillable = [
        'quantity', 'cart_id', 'product_id'
    ];

    // OneToMany relation
    public function shopping_cart() {
        return $this->belongsTo('App\ShoppingCart', 'cart_id');
    }

    // OneToMany relation
    public function product() {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
