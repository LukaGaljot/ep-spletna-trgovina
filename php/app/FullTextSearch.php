<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;

trait FullTextSearch
{
    /**
     * Scope a query that matches a full text search of term.
     *
     * @param Builder $query
     * @param string $term
     * @return Builder
     */
    public function scopeSearch($query, $term)
    {
        $columns = implode(',', $this->searchable);

        $query->selectRaw("*, MATCH ({$columns}) AGAINST (? IN BOOLEAN MODE) AS relevance", explode(' ', $term))
            ->whereRaw("MATCH ({$columns}) AGAINST (? IN BOOLEAN MODE)", $term)
            ->orderBy('relevance',  'desc');

        return $query;
    }
}
