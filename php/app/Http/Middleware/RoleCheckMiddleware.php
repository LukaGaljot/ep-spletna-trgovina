<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Http\Request;
use RuntimeException;
use function abort;
use function count;
use function preg_match;

class RoleCheckMiddleware
{
    /**
     * The authentication factory instance.
     *
     * @var Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  Factory  $auth
     * @return void
     */
    public function __construct(Factory $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        $user = $this->auth->guard('web')->user();

        if (!$request->secure()) {
            abort(400, 'Osebni certifikat zahteva HTTPS.');
        }

        if (!static::trustUser($request, $user)) {
            abort(401, "Nimate pravic za dostop do strani!");
        }

        return $next($request);
    }

    /**
     * Gets the user from cert.
     *
     * @param  Request $request
     * @return bool
     *@throws RuntimeException
     */
    protected static function trustUser(Request $request, $user)
    {
        if (empty($subject = $request->server('SSL_CLIENT_S_DN'))) {
            throw new RuntimeException('Manjka SSL_CLIENT_S_DN. Prosim, ponudite nam certifikat.');
        }

        $email = self::getEmailFromDn($subject);

        if ($email == $user->email) {
            return true;
        }

        return false;
    }

    /**
     * Parses the email address from client cert subject.
     *
     * @param  string $subject
     * @return string
     */
    protected static function getEmailFromDn(string $subject): string
    {
        preg_match('/emailAddress=([\w\+]+@[a-z\-\d]+\.[a-z\-\.\d]{2,})/i', $subject, $match);

        /**
         * emailAddress must be set.
         * @see http://www.ietf.org/rfc/rfc2459.txt
         */
        if (empty($match) || count($match) < 2) {
            return abort(400, 'Missing or invalid emailAddress in subject certificate');
        }

        return $match[1];
    }
}
