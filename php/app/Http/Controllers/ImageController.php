<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    // https://www.youtube.com/watch?v=AL8PCThJ9c4
    // run: php artisan storage:link
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(Request $request) {
        $this->validate($request, [
            'image' => 'image|max:1999',
            'product_id' => 'required'
        ]);

        if ($request->hasFile('image')) {
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $filenameToStore = $filename . '_' . time() . '.' . $extension;
            $path = $request->file('image')->storeAs('public/images', $filenameToStore);
        } else {
            $filenameToStore = 'noimage.jpg';
        }

        Image::create([
                'image' => $filenameToStore,
                'product_id' => $request->input('product_id')
            ]);;
        $user = auth()->user();
        $role = $user->role;
        if ($role->id == 3) {
            Log::info("User {$user->name} [admin] added new picture.");
        } else if ($role->id == 2) {
            Log::info("User {$user->name} [seller] added new picture..");
        }
        return redirect("/products/".$request->input('product_id'));
    }

    public function delete($id){

        $img = Image::find($id);
        $path_to_img = "images/".$img->image;
        $exists = Storage::disk('public')->has($path_to_img);
        if($exists){
            Storage::disk('public')->delete($path_to_img);
        }
        $product = $img->product_id;
        $img->delete();
        $user = auth()->user();
        $role = $user->role;
        if ($role->id == 3) {
            Log::info("User {$user->name} [admin] deleted picture.");
        } else if ($role->id == 2) {
            Log::info("User {$user->name} [seller] deleted picture..");
        }
        return redirect("product/".$product."/pictures");
    }
}
