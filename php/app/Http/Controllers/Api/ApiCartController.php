<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\ShoppingCartItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\ValidationException;

class ApiCartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getCart()
    {
        $items = auth()->user()->shopping_cart->cart_items;
        foreach ($items as $item) {
            $item->product;
        }
        return response()->json(auth()->user()->shopping_cart);
    }

    public function addItemToCart(Request $request)
    {
        try {
            $this->validate($request, [
                'product_id' => 'required',
            ]);
        } catch (ValidationException $e) {
            return Response::json("product_id is missing", 400);
        }

        $product_id = $request->input('product_id');

        $item = auth()->user()->shopping_cart->cart_items->where('product_id', $product_id)->first();
        if ($item) {
            $item->quantity += 1;
            $item->save();
        } else {
            $item = new ShoppingCartItem();
            $item->cart_id = auth()->user()->cart_id;
            $item->product_id = $product_id;
            $item->quantity = 1;
            $item->save();
        }

        return Response::json("OK", 200);
    }

    public function removeItemFromCart(Request $request)
    {
        $cart_item_id = $request->input('cart_item_id');
        $product = ShoppingCartItem::find($cart_item_id);
        try {
            $product->delete();
            return Response::json("OK", 204);
        } catch (ValidationException $e){
            return Response::json($e, 400);
        }
    }

    public function updateCartItem(Request $request, $cart_item_id)
    {
        $this->validate($request, [
            'quantity' => 'required'
        ]);

        $quantity = $request->input('quantity');;

        if ($quantity <= 0) {
            return Response::json("Negative values not allowed", 400);
        }

        try{
            $item = ShoppingCartItem::find($cart_item_id);
            $item->quantity =
            $item->save();
            return Response::json("OK", 200);
        } catch (ValidationException $e){
            return Response::json($e, 400);
        }

    }
}
