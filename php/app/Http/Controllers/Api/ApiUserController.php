<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ApiUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getUser()
    {
        return Auth::guard('api')->user();
    }

    public function updateUser(Request $request)
    {
        $user = (json_decode($request->getContent(), true));
        $curruser = User::find($user['id']);
        if($user['id'] != null && $this->getUser()->id == $user['id']){
            $curruser->name = $user['name'];
            $curruser->email = $user['email'];
            $curruser->phone = $user['phone'];
            $curruser->save();
            return Response::json("OK", 200);
        } else{
           return Response::json("BAD REQUEST", 400);
        }
    }
}
