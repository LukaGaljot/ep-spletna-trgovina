<?php

namespace App\Http\Controllers;

use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('profile/profile')->with('title', "Profil");
    }

    public function edit_info()
    {
        return view('profile/edit_profile');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|digits:9',
        ]);
        $current_user = auth()->user();
        $user = User::find($request->input('id'));
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->save();

        return redirect('/profile')->with('success', 'Profile updated');
    }
    public function updateUser($id, Request $request)
    {
        $role = auth()->user()->role;
        $current_user = auth()->user();
        if($role->id == 2){
            $this ->validate($request,[
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required|digits:9'
            ]);
            $user = User::where('id',$id)->get()[0];
            Log::info("User {$current_user->name} [seller] changed properties for customer {$user->name} with id:{$user->id}. Name: from {$user ->name} to {$request->input('name')}, email: from {$user ->email} to {$request->input('email')}, phone: from {$user ->phone} to {$request->input('phone')}.");
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->phone = $request->input('phone');
            $user->save();
            return redirect('/dashboard')->with('success','user updated');
        }
        else if($role->id == 3){
            $this ->validate($request,[
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required|digits:9',
                'role' => 'required',
            ]);
            $user = User::where('id',$id)->get()[0];
            Log::info("User {$current_user->name} [admin] changed properties for customer {$user->name} with id:{$user->id}. Name: from {$user ->name} to {$request->input('name')}, email: from {$user ->email} to {$request->input('email')}, phone: from {$user ->phone} to {$request->input('phone')}, role: from {$user->role_id} to {$request->input('role')}.") ;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->phone = $request->input('phone');
            $user->role_id=$request->input('role');
            $user->save();
            return redirect('/dashboard')->with('success','user updated');
        }
    }

    public function toggleActivity(Request $request) {
        $this->validate($request, [
            'action' => 'required',
            'user_id' => 'required',
        ]);

        $user = User::find($request->input('user_id'));
        $current_user = auth()->user();

        // Ce je user admin in enak sam sebi
        if (auth()->user()->role->id == 3 && $user->id == auth()->user()->id) {
            $users = User::orderBy('name', 'asc')->paginate(10);
            return view('dashboard.users_list')->with([
                'title' => 'Upravljaj z uporabniki',
                'error' => 'Ne moreš ukiniti samega sebe',
                'users' => $users,
            ]);
        } else if (auth()->user()->role->id == 3 || (auth()->user()->role->id == 2 && $user->role->id == 1)) {
            // Admin lahko dela kar hoce, prodajalec pa lahko upravlja samo s strankami
            if ($request->input('action') == 'deactivate') {
                if($current_user->role_id == 3){
                    if($user->role->id==2){
                        Log::info("User {$current_user->name} [admin] deactivated seller {$user->name} with id:{$user->id}.");
                    }else{
                        Log::info("User {$current_user->name} [admin] deactivated customer {$user->name} with id:{$user->id}.");
                    }
                }else if($current_user->role_id==2){
                        Log::info("User {$current_user->name} [seller] deactivated customer {$user->name} with id:{$user->id}.");
                }
                $user->active = false;
            } else {
                if($current_user->role_id == 3){
                    if($user->role->id==2){
                        Log::info("User {$current_user->name} [admin] activated seller {$user->name} with id:{$user->id}.");
                    }else{
                        Log::info("User {$current_user->name} [admin] activated customer {$user->name} with id:{$user->id}.");
                    }
                }else if($current_user->role_id==2){
                        Log::info("User {$current_user->name} [seller] activated customer {$user->name} with id:{$user->id}.");
                }
                $user->active = true;
            }
            $user->save();
        } else {
            $users = User::orderBy('name', 'asc')->paginate(10);
            return view('dashboard.users_list')->with([
                'title' => 'Upravljaj z uporabniki',
                'error' => 'Nimaš privilegijev za to dejanje',
                'users' => $users,
            ]);
        }
        if(auth()->user()->role->id == 3){
            $users = User::orderBy('name', 'asc')->paginate(10);
                return view('dashboard.users_list')->with([
                    'title' => 'Upravljaj z uporabniki',
                    'success' => 'Uspešno spremenil uporabnika',
                    'users' => $users,
                ]);
        }else if(auth()->user()->role->id == 3){
            $users = User::orderBy('name', 'asc')->with('role_id',2)->paginate(10);
                            return view('dashboard.sellers_list')->with([
                                'title' => 'Upravljaj z uporabniki',
                                'success' => 'Uspešno spremenil uporabnika',
                                'users' => $users,
                            ]);
        }
    }
}
