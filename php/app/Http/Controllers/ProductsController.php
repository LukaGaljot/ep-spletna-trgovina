<?php

namespace App\Http\Controllers;

use App\Product;
use App\Rating;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')->where('active', true)->paginate(9);
        return view('products.index')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Show the results page.
     *
     * @param Request $request
     * @param $query
     * @return Factory|View
     * @throws ValidationException
     */
    public function search(Request $request) {
        $this->validate($request, [
            'term' => 'required'
        ]);

        try {
            $results = Product::search($request->input('term'))->get();
        } catch (\Exception $e) {
            $results = [];
        }

        return view('products.search_result')->with([
            'query' => $request->input('term'),
            'products' => $results,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
        ]);

        // Create product
        $product = new Product();
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->description = $request->input('description');
        $product->save();
        $user = auth()->user();
        $role = $user->role;
        if ($role->id == 3) {
            Log::info("User {$user->name} [admin] added new product.");
        } else if ($role->id == 2) {
            Log::info("User {$user->name} [seller] added new product.");
        }
        return redirect('/products')->with('success', 'Product created');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $user = auth()->user();
        $role = $user->role;
        if ($role->id == 3) {
            Log::info("User {$user->name} [admin] edited a product.");
        } else if ($role->id == 2) {
            Log::info("User {$user->name} [seller] edited a product.");
        }
        return view('products.edit')->with('product', $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        if($request->input('action') == 'activated' || $request->input('action') == 'deactivated'){
            $product = Product::find($id);
            $product->active = $request->input('active');
            $product->save();
            return redirect('/products/'.$product->id)->with('success', 'Product '.$request->input('action'));
        }
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric',
            'description' => 'required'
        ]);

        // Update product
        $product = Product::find($id);
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->description = $request->input('description');
        $product->save();
        $user = auth()->user();
        $role = $user->role;
        if ($role->id == 3) {
            Log::info("User {$user->name} [admin] updated a product.");
        } else if ($role->id == 2) {
            Log::info("User {$user->name} [seller] updated a product.");
        }
        return redirect('/products')->with('success', 'Product updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return RedirectResponse|Redirector
     */
    public function destroy($id)
    {
        $user = auth()->user();
        $role = $user->role;
        if ($role->id == 3) {
            Log::info("User {$user->name} [admin] added new picture.");
        } else if ($role->id == 2) {
            Log::info("User {$user->name} [seller] added new picture..");
        }
        $product = Product::find($id);
        $product->delete();
        $user = auth()->user();
        $role = $user->role;
        if ($role->id == 3) {
            Log::info("User {$user->name} [admin] deleted a picture.");
        } else if ($role->id == 2) {
            Log::info("User {$user->name} [seller] deleted a picture.");
        }
        return redirect('/products')->with('success', 'Product deleted');
    }

    public function rate(Request $request, $id) {
        $res = json_decode($request->getContent(), true);
        $value = $res['value'];

        if ($res == null || $value == null || $value < 1 || $value > 5) {
            return Response::json('Bad request', 400);
        }

        $rating = Rating::where('product_id', $id)->where('user_id', auth()->user()->id)->first();

        if ($rating != null) {
            $rating->value = $value;
            $rating->save();
        } else {
            Rating::create([
                'value' => $value,
                'product_id' => $id,
                'user_id' => auth()->user()->id
            ]);
        }

        return response()->json([
            'value' => Product::find($id)->rating()
        ]);
    }

    public function pictures($id){
        $product = Product::find($id);
        return view("products.pictures")->with(["product"=>$product, "title"=>"Urejanje slik"]);

    }
    public function getRating($id) {
        return response()->json([
            'value' => Product::find($id)->rating()
        ]);
    }
}
