<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Facades\Log;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.x509.roles');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        if (auth()->user()->role->id < 2) {
            return redirect('/')->with('error', 'Nimate pravic za dostop do strani');
        }

        return view('dashboard/dashboard')->with(['title' => 'Nadzorna plošča','user'=>auth()->user()]);
    }

    public function getAllLogs()
    {
        if (auth()->user()->role->id < 2) {
            return redirect('/')->with('error', 'Nimate pravic za dostop do strani');
        }

        $list_of_logs = array();
        $list_of_paths = array();
        $log_directory = storage_path() . "/logs";
        $current_user = auth()->user();

        Log::info("User {$current_user->name} [admin] is retrieving list of all logs.");

        foreach (glob($log_directory . '/*.log') as $file) {
            $temp = explode("/", $file);
            $list_of_logs[] = end($temp);
            $list_of_paths[] = $file;
        }

        $list_of_logs = array_reverse($list_of_logs);
        return view('dashboard.logs')->with([
            'title' => 'Dnevnik',
            'logs' => $list_of_logs,
            'paths' => $list_of_paths
        ]);
    }

    public function readLog($name)
    {
        if (auth()->user()->role->id < 2) {
            return redirect('/')->with('error', 'Nimate pravic za dostop do strani');
        }

        $file = storage_path() . "\\" . "logs" . "\\" . $name;
        $lines = array();
        $current_user = auth()->user();

        Log::info("User {$current_user->name} [admin] is reading log {$name}.");

        foreach (file($file) as $line) {
            $lines[] = $line;
        }

        $lines = array_reverse($lines);
        return view('dashboard.log')->with([
            'title' => $name,
            'contents' => $lines
        ]);
    }

    public function getCustomers()
    {
        $role = auth()->user()->role;
        $user = auth()->user();
        if (auth()->user()->role->id < 2) {
            return redirect('/')->with('error', 'Nimate pravic za dostop do strani');
        }

        $customers = User::orderBy('name', 'asc')->where('role_id', 1)->paginate(10);
        if ($role->id == 3) {
            Log::info("User {$user->name} [admin] is retrieving list of all customers.");
        } else if ($role->id == 2) {
            Log::info("User {$user->name} [seller] is retrieving list of all customers.");
        }
        return view('dashboard.customers_list')->with([
            'title' => 'Upravljaj s strankami',
            'customers' => $customers,
        ]);
    }

    public function getUser($id)
    {
        if (auth()->user()->role->id < 2) {
            return redirect('/')->with('error', 'Nimate pravic za dostop do strani');
        }

        $user_to_change = User::where('id', $id)->get();
        $current_user = auth()->user();
        if ($current_user->role_id == 3) {
            Log::info("User {$current_user->name} [admin] is retrieving data of {$user_to_change[0]->name} with id: {$user_to_change[0]->id}.");
        } else if ($current_user->role_id == 2) {
            Log::info("User {$current_user->name} [seller] is retrieving data of {$user_to_change[0]->name} with id: {$user_to_change[0]->id}.");
        }
        return view('dashboard.change_user_data')->with([
            'user' => $user_to_change[0],
            'title' => "Urejanje uporabnika",
            'role' => auth()->user()->role->id
        ]);
    }

    public function getSellers()
    {
        if (auth()->user()->role->id < 2) {
            return redirect('/')->with('error', 'Nimate pravic za dostop do strani');
        }

        $role = auth()->user()->role;
        $current_user = auth()->user();

        if ($role->id != 3) {
            // redirect back
            return redirect('/dashboard')->with('error', 'Nimate pravic za ogled strani');
        }

        $sellers = User::orderBy('name', 'asc')->where('role_id', 2)->paginate(10);
        if ($role->id == 3) {
            Log::info("User {$current_user->name} [admin] is retrieving list of all sellers.");
        }
        return view('dashboard.sellers_list')->with([
            'title' => 'Upravljaj s prodajalci',
            'sellers' => $sellers,
        ]);

    }

    public function getAll()
    {
        if (auth()->user()->role->id < 2) {
            return redirect('/')->with('error', 'Nimate pravic za dostop do strani');
        }

        $role = auth()->user()->role;
        $current_user = auth()->user();
        if ($role->id != 3) {
            // redirect back
            return redirect('/dashboard')->with('error', 'Nimate pravic za ogled strani');
        }

        $users = User::orderBy('name', 'asc')->paginate(10);
        Log::info("User {$current_user->name} [admin] is retrieving list of all users.");
        return view('dashboard.users_list')->with([
            'title' => 'Upravljaj z uporabniki',
            'users' => $users,
        ]);

    }
}
