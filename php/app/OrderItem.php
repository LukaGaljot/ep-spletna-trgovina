<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    // Table name
    protected $table = 'order_items';

    protected $fillable = [
        'quantity', 'price', 'name', 'order_id', 'product_id'
    ];

    // ManyToOne relation
    public function order() {
        return $this->belongsTo('App\Order', 'order_id');
    }

    // ManyToOne relation
    public function product() {
        return $this->belongsTo('App\Product', 'product_id');
    }

    public function getPriceSum() {
        return $this->price * $this->quantity;
    }
}
