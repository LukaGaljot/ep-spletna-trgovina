var id = window.location.href.split("/").pop();
const stars = document.getElementById("stars");
var error = null;
for(let i=0;i<5;i++){
    let temp = document.createElement('BUTTON');
    temp.setAttribute('value',i);
    temp.innerHTML="<span><i class='fa fa-star-o'></i></span>";
    stars.appendChild(temp);
}

$(document).ready(function(){
    var stars = document.getElementById("stars");
    var id = window.location.href.split("/").pop();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var currentValue=$.ajax({
        type: "GET",
        url: '/product/rate/'+id,
        contentType: "application/json; charset=utf-8",
        accept: "application/json",
        success: function (response) {
            currentValue = response;
            for(let j=0;j<response.value;j++){
                stars.childNodes[j].childNodes[0].childNodes[0].classList.add('fa-star');
                stars.childNodes[j].childNodes[0].childNodes[0].classList.remove('fa-star-o');
            }
            for(let j=response.value;j<5;j++){
                stars.childNodes[j].childNodes[0].childNodes[0].classList.add('fa-star-o');
                stars.childNodes[j].childNodes[0].childNodes[0].classList.remove('fa-star');
            }
            return response;
        },
        error:function (err,ajaxOptions, thrownError) {
            if(err.status===401){
               this.error = 401;
            }
        }
    });
    for(let i =0;i<5;i++){
        document.getElementById("stars").childNodes[i].addEventListener('mouseover',function () {
            for(let j=0;j<=i;j++){
                stars.childNodes[j].childNodes[0].childNodes[0].classList.add('fa-star');
                stars.childNodes[j].childNodes[0].childNodes[0].classList.remove('fa-star-o');
            }
            for(let j=i+1;j<5;j++){
                stars.childNodes[j].childNodes[0].childNodes[0].classList.add('fa-star-o');
                stars.childNodes[j].childNodes[0].childNodes[0].classList.remove('fa-star');
            }
        });
        document.getElementById("stars").childNodes[i].addEventListener('mouseleave',function () {
            for(let j=0;j<currentValue.value;j++){
                stars.childNodes[j].childNodes[0].childNodes[0].classList.add('fa-star');
                stars.childNodes[j].childNodes[0].childNodes[0].classList.remove('fa-star-o');
            }
            for(let j=currentValue.value;j<5;j++){
                stars.childNodes[j].childNodes[0].childNodes[0].classList.add('fa-star-o');
                stars.childNodes[j].childNodes[0].childNodes[0].classList.remove('fa-star');
            }
        });
        document.getElementById("stars").childNodes[i].addEventListener('click',function(e) {
            var value = parseInt(document.getElementById("stars").childNodes[i].value)+1;
            console.log(value);
            $.ajax({
                type: "POST",
                url: '/product/rate/'+id,
                contentType: "application/json; charset=utf-8",
                accept: "application/json",
                data: JSON.stringify({"value": value}),
                success: function (response) {
                    currentValue = response;
                    console.log(response);
                    for(let j=0;j<currentValue.value;j++){
                        stars.childNodes[j].childNodes[0].childNodes[0].classList.add('fa-star');
                        stars.childNodes[j].childNodes[0].childNodes[0].classList.remove('fa-star-o');
                    }
                    for(let j=currentValue.value;j<5;j++){
                        stars.childNodes[j].childNodes[0].childNodes[0].classList.add('fa-star-o');
                        stars.childNodes[j].childNodes[0].childNodes[0].classList.remove('fa-star');
                    }
                },
                error: function (error) {
                    if(error.status===401){
                        window.alert("Please login");
                        window.location.href="/login";
                    }
                }
            });
        });
    }
});

