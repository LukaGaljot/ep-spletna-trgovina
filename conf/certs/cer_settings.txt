#izvedemo ukaze:
sudo a2enmod ssl
sudo a2enmod rewrite
sudo a2ensite default-ssl-conf

# certifikate kopiramo v mapo /etc/apache2/ssl
cp ep_cert_agency_crl.pem ep_cert_agency.crt localhost.pem /etc/apache2/ssl


# datoteka /etc/apache2/sites-available/default-ssl.conf 
# v  <VirtualHost _default_:443>

SSLCARevocationFile /etc/apache2/ssl/ep_cert_agency_crl.pem
SSLCertificateFile /etc/apache2/ssl/localhost.pem
SSLCACertificateFile /etc/apache2/ssl/ep_cert_agency.crt
SSLCARevocationCheck chain


Alias /netbeans /home/ep/NetBeansProjects
 <Directory /home/ep/NetBeansProjects>
         Require all granted
         AllowOverride all
 </Directory>

#ukaz
sudo service apache2 restart
