EP Spletna trgovina

Prijavni podatki
Administratorji:
	luka@ep.si geslo: mocnogeslo

Prodajalci:
	jana@ep.si geslo: mocnonigeslo
	rok@ep.si geslo: 12345678

Stranka:
	david@ep.si  geslo: kaksnogeslo

Za uporabniške certifikate uporabite geslo 12345678

Za vzpostavitev projekta uporabite priloženo datoteko install_project.sh
Datoteko izvršite v mapi conf.
